import { Component, OnInit,Input } from '@angular/core';
import { UtilitiesService } from '../../services/utilities.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() commentJson:any;
  constructor(private utilities:UtilitiesService) { }

  ngOnInit() {
  }

  clickUser(user){
  	this.utilities.openLink(user.html_url);
  }

}

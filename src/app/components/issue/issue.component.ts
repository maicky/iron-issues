import { Component, OnInit,Input } from '@angular/core';
import { UtilitiesService } from '../../services/utilities.service';


@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {
  @Input() issueJson:any;
  @Input() listener:any;
  constructor(private utilities:UtilitiesService) { }

  ngOnInit() {
  	this.issueJson.mostrar=false;
  }

  mostrar(issueJson){
  	issueJson.mostrar=!issueJson.mostrar;
  }

  clickUser(user){
  	this.utilities.openLink(user.html_url);
  }

  clickComments(issueJson){
    if(issueJson.comments>0){
      this.listener.clickComments(issueJson.comments_url);
    }
  }

  clickLabel(label){
    this.listener.clickLabel(label);
  }

}

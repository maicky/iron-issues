import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { StartComponent } from './pages/start/start.component';
import { RepositoryIssuesComponent } from './pages/repository-issues/repository-issues.component';
import { IssueComponent } from './components/issue/issue.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommentComponent } from './components/comment/comment.component';
import * as $ from 'jquery';
import * as bootstrap from 'bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    RepositoryIssuesComponent,
    IssueComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

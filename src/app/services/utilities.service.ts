import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  openLink(link){
  	window.open(link,"_blank");
  }

  getMaxPage(links){
  	let aLinks=links.split(",");
    console.log(aLinks);
  	for(let i=0;i<aLinks.length;i++){
  		let aLinkParts=aLinks[i].split(";")[1];

  		if(aLinkParts.trim()=="rel=\"last\""){
  			return aLinks[i].split(";")[0].split("?page=")[1].split("&")[0];
  		}
  	}
  	return -1;

  }
}

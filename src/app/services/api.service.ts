import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  urlGitHub:string="https://api.github.com/repos/";
  pageSize=10;

  constructor(private http: HttpClient) { }


  /////////////////////////////
  ////////REPOSITORIOS////////
  /////////////////////////////

  getRepository (repo): Observable<any> {
    return this.http.get<any>(this.urlGitHub+repo, {observe: 'response'});
  }

  getIssues(repo,numPage,state,issueLabel): Observable<any> {
    return this.http.get<any>(this.urlGitHub+repo+"/issues?page="+numPage+"&per_page="+this.pageSize+"&state="+state+"&labels="+issueLabel, {observe: 'response'});
  }

  getComments(commentsUrl): Observable<any> {
    return this.http.get<any>(commentsUrl);
  }

}

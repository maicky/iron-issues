import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { UtilitiesService } from '../../services/utilities.service';

import { RouterModule, Routes ,Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-repository-issues',
  templateUrl: './repository-issues.component.html',
  styleUrls: ['./repository-issues.component.css']
})
export class RepositoryIssuesComponent implements OnInit {
  maxPageButtons=8;
  issues=[];
  url:string="";
  repository:string="";
  actPage=1;
  maxPages=0;
  pages=[];
  botonMin=0;
  botonMax=0;
  issueStates="open";
  issueLabel="";
  commentsJson=[];

  constructor(public api:ApiService,
          private utilities:UtilitiesService,
  			  private router:Router,
          private route: ActivatedRoute) {

     this.url=decodeURIComponent(this.route.snapshot.paramMap.get('url')); 
   }

  ngOnInit() {
    this.getRepository();
  	this.getIssues();
  }

  getRepository(){
    this.api.getRepository(this.url)
    .subscribe(resp =>{ 
        
        this.repository=resp.body;
     }, (err) => {
          alert("Error");
      });
  }

  update(){
    this.actPage=1;
    this.getIssues();
  }


  getIssues(){
    this.issues=[];
  	this.api.getIssues(this.url,this.actPage,this.issueStates,this.issueLabel)
    .subscribe(resp =>{
        let pMaxPages=this.utilities.getMaxPage(resp.headers.get('link'));
        if(pMaxPages>0){
          this.maxPages=pMaxPages;
        }
        this.showPagesController();
        this.issues=resp.body;
        console.log(this.issues);


     }, (err) => {
          alert("Error");
      });
  }

  showPagesController(){
    this.pages=[];
    this.botonMin=Math.max(1,this.actPage-this.maxPageButtons/2);
    this.botonMax=Math.min(this.botonMin+this.maxPageButtons,this.maxPages);
    if(this.botonMax==this.maxPages){
      console.log("ES MAXXX");
      this.botonMin=Math.max(1,this.botonMax-this.maxPageButtons);
    }
    for(let i=this.botonMin;i<=this.botonMax;i++){
      this.pages.push(i);
    }
  }

  changePage(newPage){
    this.actPage=newPage;
    this.getIssues();
  }

  nextPage(){
    if(this.actPage<this.maxPages){
      this.changePage(this.actPage+1);
    }
  }

  previousPage(){
    if(this.actPage>1){
      this.changePage(this.actPage-1);
    }
  }

  clickLabel(label){
    this.issueLabel=label;
    this.update();
  }

  clickComments(commentsUrl){
    this.api.getComments(commentsUrl).subscribe(resp =>{ 
        this.commentsJson=resp;
        console.log(this.commentsJson);
     }, (err) => {
          alert("Error");
      });
    $('#modalComentarios').modal('show');
  }

  

}

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { RouterModule, Routes ,Router} from '@angular/router';


@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {
  repo:string="rails/rails";

  constructor(public api:ApiService,
  			  private router:Router) { }

  ngOnInit() {

  }

  clickStart(){
  	this.api.getRepository(this.repo)
    .subscribe(resp =>{ 
        
        this.router.navigate(['/repository/'+encodeURIComponent(this.repo)]);
     }, (err) => {
          alert("Error");
      });
  }



}

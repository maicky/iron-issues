import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StartComponent }  from './pages/start/start.component';
import { RepositoryIssuesComponent }  from './pages/repository-issues/repository-issues.component';


const routes: Routes = [
	 
	{ path: '', component: StartComponent },
	{ path: 'repository/:url', component: RepositoryIssuesComponent },

	  



];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}
